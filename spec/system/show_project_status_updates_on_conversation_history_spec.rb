require "rails_helper"

RSpec.describe "Comments on Conversation History", type: :system do
  before do
    driven_by(:rack_test)
  end

  it "allows to create and shows project status updates as entries on a conversation history" do
    visit root_path

    # First, let's create a project
    click_link "Projects"
    click_link "New project"

    fill_in "Name", with: "Test Project"
    click_button "Create"
    expect(page).to have_content "Project was successfully created."
    expect(page).to have_content "Test Project"

    # Now change the status for the project by going to Edit
    click_link "Edit"

    select "Completed", from: "Status"
    click_button "Update"

    expect(page).to have_content "Project was successfully updated."

    #
    # Now check the timeline on the project
    click_link "Projects"
    click_link "Show this project"

    expect(page).to have_content "Conversation history"
    expect(page).to have_content "Initial → Completed"
  end
end
