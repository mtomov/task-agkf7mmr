require "rails_helper"

RSpec.describe "Comments on Conversation History", type: :system do
  before do
    # driven_by(:selenium)
    driven_by(:rack_test)
  end

  it "allows to create and shows comments as entries on a conversation history" do
    visit root_path

    # First, let's create a project
    click_link "Projects"
    click_link "New project"

    fill_in "Name", with: "Test Project"
    click_button "Create"
    expect(page).to have_content "Project was successfully created."
    expect(page).to have_content "Test Project"

    # Now create a comment for this project
    click_link "Comments"
    click_link "New comment"

    # "Test Project" should be automatically selected
    fill_in "Text", with: "Test comment"
    click_button "Create"
    expect(page).to have_content "Comment was successfully created."

    #
    # Now check the timeline on the created project
    click_link "Projects"
    click_link "Show this project"

    expect(page).to have_content "Conversation history"
    expect(page).to have_content "Test comment"
  end
end
