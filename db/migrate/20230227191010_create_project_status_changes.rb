class CreateProjectStatusChanges < ActiveRecord::Migration[7.0]
  def change
    create_table :project_status_changes do |t|
      t.references :project, null: false, foreign_key: true
      t.string :from
      t.string :to

      t.timestamps
    end
  end
end
