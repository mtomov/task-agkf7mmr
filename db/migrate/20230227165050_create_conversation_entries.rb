class CreateConversationEntries < ActiveRecord::Migration[7.0]
  def change
    create_table :conversation_entries do |t|
      t.bigint :project_id
      t.bigint :activity_id
      t.string :activity_type

      t.timestamps
    end
  end
end
