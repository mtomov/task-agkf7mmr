# Homey project conversation history task

## Task

Use Ruby on Rails to build a project conversation history. A user should be able to:

  1. leave a comment
  2. change the status of the project

The project conversation history should list comments and changes in status.

### Task focus

Create a single timeline with **both** comments and project status changes. **Question to ask colleagues**

### Task won't focus on

Projects & Comments. Functionality related to just them will be created only to the extend of supporting the main task. **Question to ask colleagues**


## Running the project

```shell
bin/setup
bin/dev
```

## Approach

1. Create base objects to work with and wire them up
   1. Projects
   1. Comments

2. Create a shared timeline model and call it `ConversationEntry`

   1. Use delegated type to be more descriptive on which models have implemented it.

3. Create a UI for shared timeline. Place it front and center on root page.

4. Rewrite `CommentsController#create` to make use of Form & Service (Transaction) type of objects, which would allow for an easy modification & unit testing

5. Create a `ConversationEntry` on create of a `Comment`

6. Update UI to show comments as as "type of" a conversation entry

7. Verify all is looking good end-to-end, and as happy with result -> solidify it with a system test

8. One thing I noticed while completing the system test is that I have created a single conversation history across all projects. Reading back at the task it says to create "project conversation history". If a single timeline across all projects was needed, I believe the task would've said "project**s** conversation history". Still worth checking with a team member. **Question to ask colleagues**
   1. I'd now make a modification to change the timeline to be per project, and it would appear on click on every project (something to certainly verify with a team member as well)

9. At this point, I'd say it'll still be interesting to show how a "Status change" could be represented on the timeline. Given that we have all the fundementals in a seemingly good place, that shouldn't be too difficult to wire up. Let's do it!

10. Create `ProjectStatusChange` to represent a change in status

11. Final bit is to add Change Status as one more Activity type on ConversationEntry and display the results in the view.

12. Finally, I'd go back to the original description of the task, and re-check that it meets the requreiments.

      I'd think that the current implementation is a good to demonstration of my approach. Even if there's issues at this point, I'd think that the project fulfills the initial goal of displaying a timeline, which is the most important thing to get right from a data and usability perspective.

      Going back to the "Please write down the questions you would have asked your colleagues" prompt from the task, I'd say that my main questions would be during the start of the project, as to what a Conversation History represents, and how do people imagine it evolving. This is represented in the first 2 **Question to ask colleagues**. I'd also check with them on the UI as I progress (3rd **Question to ask colleagues**).

      Having good decisions on those, and re-checking them as we go would dictate the data organisation and UI. I wouldn't be as worried on the actual code that does the implementation, as there's little unknowns in that area, and as such, it's a relatively low risk to make changes even towards the end of the "project" if something is not right.




## Things to consider

1. "Comments Add" interface can be better by first going to a project, then clicking on a "New comment" button from there. This would eliminate the need to have a dropdown for Project when creating a comment. Even further, a "comment add box" could be added directly underneath the conversation history page.

2. Empty state of Conversation History

3. Naming - I'd consider naming to be an important part of coding. The approach I take is to think about few names at first, but spend no more than a few minutes, and pick the best out of a few. Considering that the final approach could represent things in a much different way, it's best to think of renaming as the "easy part" that can come at a point right before shipping the code. Always good to discuss with a team member on suggestions, ideally without proposing one's ideas first to avoid bias into your colleague's thinking.

4. I'd probably create a presenter/view component for Project's status - as quite often we need to do `project.status&.titleize` which could be error prone, and in the future more requirements for it could come up. I'd even generalize the presenter/view component to a `StatusPresenter` as a status is now part of the `ProjectStatusChange` model.

5. Unit testing `CommentCreate` and `UpdateProject` as well as the 2 forms that come with them should be relatively easy. We could even use dependency injection for `ProjectStatusChange` and `ConversationEntry` for some extra fun 🙂


## Testing

```
bin/rspec spec
```

## Final result

![Final result](/uploads/40329ba134b7266be18203357c090247/out.webm)
