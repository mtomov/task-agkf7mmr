class ConversationEntry < ApplicationRecord
  belongs_to :project
  delegated_type :activity, types: %w[Comment ProjectStatusChange]

  def self.create_with_activity(activity)
    ConversationEntry.create! \
      activity: activity,
      project_id: activity.project_id
  end
end
