class Project < ApplicationRecord
  has_many :status_changes, dependent: :destroy, class_name: "ProjectStatusChange"
  has_many :comments, dependent: :destroy

  enum status: {
    initial: "initial",
    in_progress: "in_progress",
    completed: "completed"
  }
end
