module AsConversationEntry
  extend ActiveSupport::Concern

  included do
    has_one :conversation_entry, as: :activity,
      touch: true,
      dependent: :destroy
  end
end
