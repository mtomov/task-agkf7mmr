class ProjectStatusChange < ApplicationRecord
  belongs_to :project

  enum :from, Project.statuses, prefix: true
  enum :to, Project.statuses, prefix: true

  def self.create_status_change!(project:, from:, to:)
    ProjectStatusChange.create! \
      project: project,
      from: from,
      to: to
  end
end
