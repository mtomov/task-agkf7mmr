class Comment < ApplicationRecord
  include AsConversationEntry

  belongs_to :project
end
