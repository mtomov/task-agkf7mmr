module Projects
  module Loaders
    class ConversationEntriesLoader
      def call(project:)
        ConversationEntry.where(project: project)
      end
    end
  end
end
