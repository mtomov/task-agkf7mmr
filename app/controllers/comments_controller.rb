class CommentsController < ApplicationController
  before_action :set_comment, only: %i[show edit update destroy]

  # GET /comments or /comments.json
  def index
    @comments = Comment.all
  end

  # GET /comments/1 or /comments/1.json
  def show
  end

  # GET /comments/new
  def new
    form = CommentForm.new
    render locals: {form: form}
  end

  def create
    form = CommentForm.new(comment_params)
    create_comment = CreateComment.new.call(form: form)

    if create_comment.success?
      comment = create_comment.value!

      redirect_to comment_url(comment), notice: "Comment was successfully created."
    else
      render :new, status: :unprocessable_entity, form: form
    end
  end

  # DELETE /comments/1 or /comments/1.json
  def destroy
    @comment.destroy

    respond_to do |format|
      format.html { redirect_to comments_url, notice: "Comment was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private

  class CommentForm
    include ActiveModel::Model
    include ActiveModel::Attributes

    attribute :text, :string
    attribute :project_id, :integer
  end

  class CreateComment
    include Dry::Monads[:result]

    def call(form:)
      return Failure(form) unless form.valid?

      comment = Comment.new
      comment.text = form.text
      comment.project_id = form.project_id

      Comment.transaction do
        comment.save!

        ConversationEntry.create_with_activity(comment)
      end

      Success(comment)
    end
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_comment
    @comment = Comment.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def comment_params
    params.require(CommentForm.model_name.param_key).permit(:project_id, :text)
  end
end
