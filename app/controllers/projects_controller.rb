class ProjectsController < ApplicationController
  before_action :set_project, only: %i[show edit update destroy]

  # GET /projects or /projects.json
  def index
    @projects = Project.all
  end

  # GET /projects/1 or /projects/1.json
  def show
  end

  # GET /projects/new
  def new
    @project = Project.new
  end

  # GET /projects/1/edit
  def edit
    form = ProjectUpdateForm.new(@project.attributes.slice("name", "status"))

    render locals: {form: form}
  end

  # POST /projects or /projects.json
  def create
    @project = Project.new(project_params)

    respond_to do |format|
      if @project.save
        format.html { redirect_to project_url(@project), notice: "Project was successfully created." }
        format.json { render :show, status: :created, location: @project }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @project.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /projects/1 or /projects/1.json
  def update
    form = ProjectUpdateForm.new(update_project_params)
    update_project = UpdateProject.new.call(form: form, project: @project)

    if update_project.success?
      project = update_project.value!

      redirect_to project_url(project), notice: "Project was successfully updated."
    else
      render :edit, status: :unprocessable_entity, form: form
    end
  end

  # DELETE /projects/1 or /projects/1.json
  def destroy
    @project.destroy

    respond_to do |format|
      format.html { redirect_to projects_url, notice: "Project was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private

  class ProjectUpdateForm
    include ActiveModel::Model
    include ActiveModel::Attributes

    attribute :name, :string
    attribute :status, :string

    def persisted?
      true
    end
  end

  class UpdateProject
    include Dry::Monads[:result]

    def call(form:, project:)
      return Failure(form) unless form.valid?

      from_status = project.status
      to_status = form.status

      project.name = form.name
      project.status = form.status

      Project.transaction do
        project.save!

        status_change = ProjectStatusChange.create_status_change!(
          project: project,
          from: from_status,
          to: to_status
        )

        ConversationEntry.create_with_activity(status_change)
      end

      Success(project)
    end
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_project
    @project = Project.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def update_project_params
    params.require(ProjectUpdateForm.model_name.param_key).permit(:name, :status)
  end

  def project_params
    params.require(:project).permit(:name, :status)
  end
end
